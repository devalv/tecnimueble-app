import React from 'react';
import { StyleSheet, View } from 'react-native';
import { WebView } from 'react-native-webview';
/*
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const displayHeight = window.height;
const webViewHeight = displayHeight - 20;
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor: 'transparent'
  },
});

export default function App() {
  return (
    <View style={styles.container}>
      <WebView
        source={{ 
          uri: 'https://shop.tecnimueble.com.ec/' 
        }}
        style={{ 
          width: '100%',
          marginTop: 20, 
          height: '100%'
        }}
      />
    </View>
  );
}
